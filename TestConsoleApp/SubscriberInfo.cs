using System;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    public class SubscriberInfo
    {
        public SubscriberInfo(string topicId, string queueId, bool deleteOnDisconnect,
            Func<ReadOnlyMemory<byte>, ValueTask> callback)
        {
            TopicId = topicId;
            QueueId = queueId;
            DeleteOnDisconnect = deleteOnDisconnect;
            Callback = callback;
        }

        public string TopicId { get; }
        public string QueueId { get; }
        public bool DeleteOnDisconnect { get; }
        public Func<ReadOnlyMemory<byte>, ValueTask> Callback { get; }    
    }
}