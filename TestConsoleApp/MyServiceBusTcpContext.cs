using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyServiceBus.TcpContracts;
using MyTcpSockets;

namespace TestConsoleApp
{
    public class MyServiceBusTcpContext : ClientTcpContext<IServiceBusTcpContract>
    {
                private readonly Dictionary<long, TaskCompletionSource<int>> _tasks 
            = new Dictionary<long, TaskCompletionSource<int>>();

        private readonly Dictionary<string, SubscriberInfo> _subscribers;
        private readonly string _name;
        private readonly Func<IReadOnlyList<(string topicName, int maxCachedSize)>> _checkAndCreateTopicOnConnect;

        public MyServiceBusTcpContext(Dictionary<string, SubscriberInfo> subscribers, string name, 
            Func<IReadOnlyList<(string topicName, int maxCachedSize)>> checkAndCreateTopicOnConnect)
        {
            _subscribers = subscribers;
            _name = name;
            _checkAndCreateTopicOnConnect = checkAndCreateTopicOnConnect;
        }

        protected override ValueTask OnConnectAsync()
        {
            SendGreetings(_name);

            foreach (var (topicId, maxCachedSize) in _checkAndCreateTopicOnConnect())
            {
                SetCreateTopicIfNotExistsOnConnect(topicId, maxCachedSize);
            }

            foreach (var subscriber in _subscribers.Values)
            {
                SendSubscribe(subscriber.TopicId, subscriber.QueueId, subscriber.DeleteOnDisconnect);
            }
            
            return new ValueTask();
        }

        private IReadOnlyList<KeyValuePair<long, TaskCompletionSource<int>>> GetTasks()
        {
            lock (_lockObject)
            {
                _disconnected = true;
                return _tasks.ToList();
            }
        }

        private bool _disconnected;
        
        protected override ValueTask OnDisconnectAsync()
        {

            var tasks = GetTasks();

            foreach (var (key, value) in tasks)
            {
                value.SetException(new Exception("Disconnected"));
                
                lock (_lockObject)
                    if (_tasks.ContainsKey(key))
                        _tasks.Remove(key);
            }


            return new ValueTask();
        }

        protected override ValueTask HandleIncomingDataAsync(IServiceBusTcpContract data)
        {
            
            Console.WriteLine("Client: "+data.GetType());
            
            if (data is NewMessageContract newMsg)
                return NewMessageAsync(newMsg);

            if (data is PublishResponseContract pr)
                PublishResponse(pr);

            return new ValueTask();
        }
        
        private void PublishResponse(PublishResponseContract pr)
        {
            var task = RemoveTask(pr.RequestId);

            task?.SetResult(0);

        }
        
        private void SendSubscribe(string topicId, string queueId, bool deleteOnDisconnect)
        {
            var contract = new SubscribeContract
            {
                TopicId = topicId,
                QueueId = queueId,
                DeleteOnDisconnect = deleteOnDisconnect
            };
            
            SendPacket(contract);
        }

        
        public static string GetId(string topicId, string queueId)
        {
            return topicId + "|" + queueId;
        }

        private async ValueTask NewMessageAsync(NewMessageContract newMsg)
        {

            try
            {
                foreach (var msg in newMsg.Data)
                {
                    var id = GetId(newMsg.TopicId, newMsg.QueueId);

                    var subscriber = _subscribers[id];

                    await subscriber.Callback(msg.Data);
                }

                SendMessageConfirmation(newMsg); 

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        private void SendMessageConfirmation(NewMessageContract messages)
        {
            var contract = new NewMessageConfirmationContract
            {
                TopicId = messages.TopicId,
                QueueId = messages.QueueId,
                ConfirmationId = messages.ConfirmationId
            };
            
            SendPacket(contract);
        }
        
        private void SendMessageReject(NewMessageContract messages)
        {
            var contract = new NewMessageConfirmationContract
            {
                TopicId = messages.TopicId,
                QueueId = messages.QueueId,
                ConfirmationId = messages.ConfirmationId
            };
            
            SendPacket(contract);
        }
        
        
        private const int ProtocolVersion = 2; 
        
        private void SendGreetings(string name)
        {
            var contract = new GreetingContract
            {
                Name = name,
                ProtocolVersion = ProtocolVersion
            };

            SendPacket(contract);
        }

        private void SetCreateTopicIfNotExistsOnConnect(string topicId, int maxCachedSize)
        {
            var contract = new CreateTopicIfNotExistsContract
            {
                TopicId = topicId,
                MaxMessagesInCache = maxCachedSize
            };

            SendPacket(contract);
        }


        protected override IServiceBusTcpContract GetPingPacket()
        {
            return PingContract.Instance;
        }


        private static long _requestId;


        private (long taskId, TaskCompletionSource<int> Task) GetNewTask()
        {
            lock (_lockObject)
            {
                
                if (_disconnected)
                    throw new Exception("Socket is disconnected");
                
                var requestId = _requestId++;
                
                var tc = new TaskCompletionSource<int>();
                _tasks.TryAdd(requestId, tc);

                return (requestId, tc);
            }
        }
        


        private TaskCompletionSource<int> RemoveTask(long confirmationId)
        {
            lock (_lockObject)
            {

                if (_fireAndForgetTopics.ContainsKey(confirmationId))
                {
                    var topicId = _fireAndForgetTopics[confirmationId];
                    _fireAndForgetTopics.Remove(confirmationId);

                    var state = _publishAndForgetStates[topicId];

                    state.Confirmed(confirmationId);
                    SendNextDataChunk(state, topicId);
                }

                if (_tasks.ContainsKey(confirmationId))
                {
                    var task = _tasks[confirmationId];

                    if (!_tasks.ContainsKey(confirmationId)) return task;
                    _tasks.Remove(confirmationId);

                    return task;
                }
            }

            return null;
        }
        
        
        private readonly object _lockObject = new object();

        private long GetNewRequestId()
        {
            _requestId++;
            return _requestId;
        }
        
        private readonly Dictionary<string, PublishAndForgetState> _publishAndForgetStates = new Dictionary<string, PublishAndForgetState>();
        private readonly Dictionary<long, string> _fireAndForgetTopics = new Dictionary<long, string>();
        
        private PublishAndForgetState GetFireAndForgetStateOrCreate(string topicId)
        {
            if (_publishAndForgetStates.ContainsKey(topicId))
                return _publishAndForgetStates[topicId];

            var newItem = new PublishAndForgetState();
            _publishAndForgetStates.Add(topicId, newItem);
            return newItem;
        }


        private void SendNextDataChunk(PublishAndForgetState state, string topicId)
        {

            var data = state.GetMessagesToSend();
            
            if (data.Count == 0)
                return;
            
            var requestId = GetNewRequestId();
                
            var contract = new PublishContract
            {
                TopicId = topicId,
                RequestId = requestId,
                Data = data,
                ImmediatePersist = 0
            };
                
            SendPacket(contract);

            state.SetOnRequest(requestId);
            
            _fireAndForgetTopics.Add(requestId, topicId);
        }
        
        
        public void PublishFireAndForget(string topicId, IEnumerable<byte[]> data)
        {
            lock (_lockObject)
            {
                var state = GetFireAndForgetStateOrCreate(topicId);
                state.Enqueue(data);
                if (state.HasFireAndForgetRequests())
                    return;
                
                SendNextDataChunk(state, topicId);
            }
        }
        
        
        public async Task PublishAsync(string topicId, IReadOnlyList<byte[]> data, bool immediatelyPersist)
        {
            
            var contract = new PublishContract
            {
                TopicId = topicId,
                RequestId = _requestId,
                Data = data,
                ImmediatePersist = immediatelyPersist ? (byte)1 : (byte)0
            };

            var task = GetNewTask();

            try
            {
                SendPacket(contract);
                await task.Task.Task;
            }
            catch (Exception)
            {
                RemoveTask(contract.RequestId);
                throw;
            }
            
        }
    }
}