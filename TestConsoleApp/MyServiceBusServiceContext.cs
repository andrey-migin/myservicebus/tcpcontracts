using System;
using System.Threading.Tasks;
using MyServiceBus.TcpContracts;
using MyTcpSockets;

namespace TestConsoleApp
{
    public class MyServiceBusServiceContext : TcpContext<IServiceBusTcpContract>
    {
        protected override ValueTask OnConnectAsync()
        {
            return new ValueTask();
        }

        protected override ValueTask OnDisconnectAsync()
        {
            return new ValueTask();
        }

        protected override ValueTask HandleIncomingDataAsync(IServiceBusTcpContract data)
        {
            
            Console.WriteLine("Server Packet: "+data.GetType());
            
            if (data is PingContract)
            {
                Console.WriteLine("Server Ping");
                SendPacket(PongContract.Instance);
            }


            if (data is PublishContract publishContract)
            {
                SendPacket(new PublishResponseContract
                {
                    RequestId = publishContract.RequestId
                });
            }
            
            return new ValueTask();
        }
    }
}